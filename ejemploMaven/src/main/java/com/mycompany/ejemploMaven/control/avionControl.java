/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejemploMaven.control;

import com.mycompany.ejemploMaven.dao.AvionFacade;
import com.mycompany.ejemploMaven.entidad.Avion;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.ejb.EJB;

/**
 *
 * @author laptop
 */
@Named(value = "avionControl")
@SessionScoped
@ManagedBean

public class avionControl implements Serializable {

    @EJB
    private AvionFacade avionFacade;

    /**
     * Creates a new instance of avionControl
     */
    
    List<Avion> aviones;
    
    public avionControl() {
    }



    public List<Avion> getAviones() {
        
        aviones = avionFacade.findAll();
        return aviones;
    }

    public void setAviones(List<Avion> aviones) {
        this.aviones = aviones;
    }
    
}
